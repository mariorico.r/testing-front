describe('Tareas App', () => {
  it('Home renderizada', () =>{
    cy.visit('https://testing-front.mariorico.es')
    cy.contains('Listado de tareas')
  })

  it("Crear tarea", () => {
    cy.visit("/").then(() => {
      cy.get('#id').type(77);
      cy.get('#usuarioId').type(77);
      cy.get('#titulo').type("Creando tarea");
      cy.get('#fecha').type("25/09/2021");
      cy.get('#estado').type("completed");
      cy.get('.crearForm > button').click();
    });
  });

  it("Borrar Tarea", () => {
    cy.visit("/").then(() => {
      cy.get(':nth-child(2) > :nth-child(6) > button').click();
    });
  });
});
