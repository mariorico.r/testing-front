import { Injectable } from '@angular/core';
import { Tarea } from '../models/tarea';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class TareasService {

  // Propiedades
  tareas: Tarea[] = [];
  url: string='https://gorest.co.in/public/v1/todos';

  // Constructor
  constructor(private http: HttpClient) { }

  // Métodos

  // Getters & Setters
  getTareas() {
    return this.http.get(this.url, { responseType: 'json' });
  }
}
