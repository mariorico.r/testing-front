import { fakeAsync, getTestBed, TestBed } from '@angular/core/testing';
import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing'
import { TareasService } from './tareas.service';
import { AppComponent } from '../app.component';

const tareasStub = [
  {
    id: 37,
    user_id: 25,
    title:
      'Decumbo calco concedo exercitationem corpus sublime uredo in vito animi.',
    due_on: '2021-10-22T00:00:00.000+05:30',
    status: 'completed',
  },
  {
    id: 38,
    user_id: 26,
    title:
      'Corrumpo arma apparatus acceptus curiositas spiculum cursim veritatis.',
    due_on: '2021-10-21T00:00:00.000+05:30',
    status: 'completed',
  }
];

describe('TareasService', () => {
  let testBed: TestBed;
  let service: TareasService;
  let httpMock: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [TareasService],
    });

testBed = getTestBed();
httpMock = testBed.inject(HttpTestingController);
service = testBed.inject(TareasService);
});

 afterEach(() => {
  httpMock.verify();
});

it('should be created', () => {
  expect(service).toBeTruthy();
});

it('getTareas', () => {
  service.getTareas().subscribe((response) => {
    expect(response).toBe(tareasStub);
  });
  const request = httpMock.expectOne('https://gorest.co.in/public/v1/todos');
  request.flush(tareasStub);
  expect(request.request.method).toBe('GET');
});

});
