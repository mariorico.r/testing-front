import { Observable, of } from 'rxjs';

export class TareasServiceStub {
  static borrarTarea: any;
  getTareas(): Observable<any> {
    return of({
      data: [
        {
          id: 37,
          user_id: 25,
          title:
            'Decumbo calco concedo exercitationem corpus sublime uredo in vito animi.',
          due_on: '2021-10-22T00:00:00.000+05:30',
          status: 'completed',
        },
        {
          id: 38,
          user_id: 26,
          title:
            'Corrumpo arma apparatus acceptus curiositas spiculum cursim veritatis.',
          due_on: '2021-10-21T00:00:00.000+05:30',
          status: 'completed',
        }
      ],
    });
  }

}
