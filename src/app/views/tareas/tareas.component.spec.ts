import { ComponentFixture, TestBed } from '@angular/core/testing';
import { TareasComponent } from './tareas.component';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';
import { TareasService } from '../../services/tareas.service';
import { TareasServiceStub } from '../../mocks/tareas.service.mock';

describe('Test TareasComponent', () => {
  let component: TareasComponent;
  let fixture: ComponentFixture<TareasComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TareasComponent ],
      imports:[
        HttpClientModule,
        FormsModule,
        RouterTestingModule,
      ],
      providers:[
        {
          provide: TareasService,
          useClass: TareasServiceStub,
        },
      ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TareasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('Test crearTarea', () => {
    let size = component.tareas.length;
    component.crearTarea();
    expect(component.tareas.length).toEqual(size + 1);
  });

  it('Test borrarTarea ok', () => {
    let size = component.tareas.length;
    component.borrarTarea(37);
    expect(component.tareas.length).toEqual(size - 1);
  });

  it('Test borrarTarea ko', () => {
    let size = component.tareas.length;
    component.borrarTarea(1000000);
    expect(component.tareas.length).not.toEqual(size - 1);
  });

  it('test getTareas'), () => {
    expect(component.getTareas().length).toEqual(2);
  }

});
