import { Component, OnInit } from '@angular/core';
import { TareasService } from 'src/app/services/tareas.service';

@Component({
  selector: 'app-tareas',
  templateUrl: './tareas.component.html',
  styleUrls: ['./tareas.component.css']
})
export class TareasComponent implements OnInit {

  // Propiedades
  tareas: any[] = [];
  id = 0;
  usuarioId = 0;
  titulo = '';
  fecha = '';
  estado = '';

  // Constructor
  constructor(public tareasService: TareasService) { }

  // Métodos
  ngOnInit(): void {
    this.tareasService.getTareas().subscribe((respuesta: any) => (this.tareas = respuesta['data']));
  }

  crearTarea() {
    let tarea = {
      id: this.id,
      user_id: this.usuarioId,
      title: this.titulo,
      due_on: this.fecha,
      status: this.estado
    }
    this.tareas.push(tarea);
  }

  borrarTarea(id: number) {
    this.tareas.forEach((tarea: { id: number }) => {
      if (tarea.id === id) {
        this.tareas.splice(this.tareas.indexOf(tarea), 1);
      }
    });
  }

  // Getters & Setters
  getTareas(){
    return this.tareas;
  }
}
